alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'
alias private='unset HISTFILE'
alias zshrc="source ~/.zshrc"
alias gitignore="wget https://raw.githubusercontent.com/github/gitignore/master/Node.gitignore -O .gitignore"
alias as="curl https://www.adultswim.com/api/schedule/onair | jq '.data[] | [(.datetime[0:19]|strptime(\"%Y-%m-%dT%H:%M:%S\")|strftime(\"%H:%M\")), .showTitle] | join(\" \")'"
alias dev='git checkout develop && git pull'
alias dfiles='cd ~/code/dotfiles'

alias bal="node ~/code/dotfiles/scripts/bal"
alias fm='node ~/code/dotfiles/scripts/fm'
