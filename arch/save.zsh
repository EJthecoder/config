#!/sbin/zsh

chalk cyan "Running git pull..."
git pull

# Files
chalk cyan "Copying files..."
cp ~/.config/i3/config i3-config
cp ~/.config/i3blocks/config i3blocks
cp ~/.config/alacritty/alacritty.yml .
cp ~/.gitconfig ../global
cp ~/.zshrc .

# Packages
chalk cyan "Exporting package history..."
#history | grep "yay -S" | awk '{print $6}' | uniq | grep -v grep > packages/yay-history.txt
export HISTFILE=~/.zsh_history 
fc -R

history 1 | awk '{if ($2 == "yay" && $3 == "-S") print $4}' | uniq > packages/yay-history.txt
history 1 | awk '{if ($2 == "yay" && $3 !~ /^-/) print $3}' | uniq | grep -v ^$ > packages/yay-search-history.txt
#history 1 > history.txt
yay -Qqen > packages/arch-packages.txt
yay -Qqem > packages/aur-packages.txt

chalk cyan "Committing updates..."
git commit -am "Update `date +'%Y-%m-%d'` from lenovo-arch"
git --no-pager log --stat -1

