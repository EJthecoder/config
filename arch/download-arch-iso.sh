MIRROR=$(grep Server /etc/pacman.d/mirrorlist | head -1 | cut -c 10- | sed "s/\$repo\/os\/\$arch/iso/")
ISO="$(date -u +"%Y.%m").01"
curl $MIRROR/$ISO/md5sums.txt -o archlinux-md5sums
curl $MIRROR/$ISO/sha1sums.txt -o archlinux-sha1sums
curl -O $MIRROR/$ISO/archlinux-$ISO-x86_64.iso

md5sum -c archlinux-md5sums --ignore-missing
sha1sum -c archlinux-sha1sums --ignore-missing
