alias i3c='"$EDITOR" ~/.config/i3/config'
alias xrc='"$EDITOR" ~/.Xresources'
alias xrl='xrdb ~/.Xresources'
alias internalip="ip a | grep '\(10.0.0.\|192.168.\)'"
alias xclip="xclip -sel clip"
alias m1pull='ssh m1 "cd code/dotfiles && git pull"'
