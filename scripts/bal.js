const { Configuration, PlaidApi, PlaidEnvironments } = require('plaid');

const configuration = new Configuration({
  basePath: PlaidEnvironments.development,
  baseOptions: {
    headers: {
      'PLAID-CLIENT-ID': process.env.PLAID_CLIENT_ID,
      'PLAID-SECRET': process.env.PLAID_CLIENT_SECRET,
      'Plaid-Version': '2020-09-14',
    },
  },
});


const plaidClient = new PlaidApi(configuration);

// Get the access token by running the quickstart application in 'development' environment:
// https://github.com/plaid/quickstart
(async () => {
  const { accounts } = (
    await plaidClient.accountsBalanceGet({
      access_token: process.env.PLAID_ONPOINT_ACCESS_TOKEN,
    })
  ).data;

  console.log(`\nDepositories\n=====`);
  console.log(
    accounts
      .filter((account) => account.type === 'depository')
      .map(({ balances, name }) => `${name}: $${balances.available}`)
      .sort()
      .join('\n')
  );

  console.log(`\nCredit cards\n=====`);
  console.log(
    accounts
      .filter((account) => account.type === 'credit')
      .map(({ balances, name }) => `${name}: $${balances.current}`)
      .join('\n')
  );
})();
