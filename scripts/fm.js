const lastfmnode = require('lastfm').LastFmNode;

const lastfm = new lastfmnode({
  api_key: process.env.LASTFM_API_KEY,
  secret: process.env.LASTFM_API_SECRET 
});

const formatTrack = (trackInfo) => {
  let artist = trackInfo.artist['#text'];
  let album = trackInfo.album['#text'];
  let track = trackInfo.name;
  return `${artist} - ${track}`;
}

let tracks = lastfm.request('user.getRecentTracks', {
    user: 'elias-jackson2',
    handlers: {
      success: data => {
        let recentTracks = data.recenttracks.track.slice(0, 5);
        recentTracks.map(t => console.log(formatTrack(t)));
      }
    }
});
