// https://stackoverflow.com/a/13735363
function pbcopy(data) {
	const proc = require('child_process').spawn('pbcopy');
	proc.stdin.write(data);
	proc.stdin.end();
}

// https://stackoverflow.com/a/19691491
function addDays(date, days) {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

const answerFormatter = (article) => article.length === 3 ? `${article[0].replace('"', "'")} (${article[1]} | ${article[2]})` : `${article[0].replace('"', "'")} (${article[1]})`;

const pollmasterCmd = 'pm!cmd';
const pollEndDate = addDays(new Date(), 1)
const pollEndDateStr = `${pollEndDate.getMonth() + 1}/${pollEndDate.getDate()} 11:59pm PST`;
const discussionDate = addDays(new Date(), 7)
const discussionDateStr = `${discussionDate.getMonth()+1}/${discussionDate.getDate()}`

// Pollmaster's limit seems to be 7 options
const articles = [
	['Anti-Capitalist Teens Are Sharing Shoplifting Tips on TikTok', 'Vice', 'Kate Fowler'],
	['COVID-19: Democratic Voters Support Harsh Measures Against Unvaccinated', 'Rasmussen Reports'],
	["What Silicon Valley 'Gets' about Software Engineers that Traditional Companies Do Not", 'The Pragmatic Engineer', 'Gergely Orosz'],
	["It Doesn't Make Sense to Treat Facebook Like a Public Utility", 'Wired', 'Os Keyes'],
	['Meet the Biden and Trump voters who switched parties in the 2020 election', 'Vox', 'Brianna Provenzano'],
	['COVID-19 vaccine booster drive is faltering in the US', 'AP', 'Mae Anderson'],
	['10 Myths About Big Tech & Antitrust', 'Progressive Policy Institute', 'Alec Stapp'],
];

// Validate articles array
for (const article of articles) {
	if (article.length !== 2 && article.length !== 3) {
		throw new Error(`Invalid number of strings for "${answerFormatter(article)}" (Expected 2 or 3, got ${article.length})`);
	}
	if (article.join('').indexOf(',') !== -1) {
		throw new Error(`Comma found in "${answerFormatter(article)}"`);
	}
}

const answers = articles.map(answerFormatter).join(',');

const fullCommand = `${pollmasterCmd} -question "Article Discussion ${discussionDateStr}" -mc 0 -deadline "${pollEndDateStr}" -o "${answers}"`;

pbcopy(fullCommand);
console.log('Command copied to clipboard:');
console.log(fullCommand);

// const { execSync, spawn } = require('child_process');
// const { extract } = require('article-parser');
// const { kebabCase } = require('change-case');
// const fs = require('fs-extra');

// function exec(command) {
// 	return new Promise(function (resolve, reject) {
// 		execSync(command, (error, stdout, stderr) => {
// 			if (error) {
// 				reject(error);
// 				return;
// 			}

// 			resolve(stdout.trim());
// 		});
// 	});
// }

// // https://stackoverflow.com/a/13735363
// function pbcopy(data) {
// 	const proc = spawn('pbcopy');
// 	proc.stdin.write(data);
// 	proc.stdin.end();
// }
// const answerFormatter = (article) =>
// 	`[${article.customTitle}](${article.url}) (${article.siteUrl} | ${article.author})`;

// const pollmasterCmd = 'pm!cmd';
// const discussionHourPM = 6;
// const pollEndDate = `${new Date().getMonth() + 1}/${new Date().getDate() + 1} ${discussionHourPM}pm PST`;
// const discussionDate = `${new Date().getMonth() + 1}/${new Date().getDate() + 7}`;

// const articles = [
// 	{
// 		url: 'https://www.nytimes.com/2022/01/07/opinion/inflation-2022-us-economy.html',
// 		title: 'High Inflation May Not Disappear in 2022',
// 	},
// 	{
// 		url: 'https://www.washingtonpost.com/outlook/2021/09/24/working-home-productivity-pandemic-remote/',
// 		title: "You may get more work done at home. But you'd have better ideas at the office.",
// 	},
// ];

// (async () => {
// 	console.log(await exec('ls'));
// 	// console.log(await exec('which pandoc'));
// 	// console.log(await exec('which ffsend'));
// 	// console.log(await fs.readdir('.'));

// 	console.log('test');
// 	// const answersArr = [];
// 	// for (const article of articles) {
// 	// 	console.log(article);
// 	// 	const articleData = await extract(article.url);
// 	// 	articleData.customTitle = article.title;
// 	// 	articleData.siteUrl = articleData.url.split('/')[2].replace('www.', '');

// 	// 	const pdfFilename = `${kebabCase(articleData.title)}.pdf`;
// 	// 	const pdfSaver = spawn(`pandoc -r html -o articles/${pdfFilename}`);
// 	// 	pdfSaver.stdin.write(articleData.content);
// 	// 	pdfSaver.stdin.end();

// 	// 	articleData.pdfUrl = await exec(`ffsend upload --expiry-time 7f ${pdfFilename}`);

// 	// 	// TODO: Validation
// 	// 	answers.push(answerFormatter(articleData));
// 	// }
// 	// const answers = answersArr.join(',');

// 	// // Validate articles array
// 	// // for (const article of newArticles) {
// 	// // 	if (article.length !== 2) {
// 	// // 		throw new Error(
// 	// // 			`Invalid number of strings for "${answerFormatter(article)}" (Expected 3, got ${article.length})`
// 	// // 		);
// 	// // 	}
// 	// // 	if (article.join('').indexOf(',') !== -1) {
// 	// // 		throw new Error(`Comma found in "${answerFormatter(article)}"`);
// 	// // 	}
// 	// // }

// 	// // const answers = articles.map(answerFormatter).join(',');

// 	// const fullCommand = `${pollmasterCmd} -question "Article Discussion ${discussionDate}" -mc 0 -deadline "${pollEndDate}" -o "${answers}"`;

// 	// pbcopy(fullCommand);
// 	// console.log('Command copied to clipboard:');
// 	// console.log(fullCommand);

// 	// ffsend upload --expiry-time 7d
// })();
