#!/bin/zsh

echo "Running git pull..."
git pull

# Files
echo "Copying files..."
cp ~/.zshrc .
cp ~/Library/Application\ Support/REAPER/*.ini ./REAPER

# Packages
echo "Exporting package history..."
brew leaves > packages/brew-leaves.txt
mas list > packages/mas-list.txt

echo "Committing updates..."
git commit -am "Update `date +'%Y-%m-%d'` from m1"
git status
